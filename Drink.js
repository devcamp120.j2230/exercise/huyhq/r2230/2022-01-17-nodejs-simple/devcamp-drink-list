class Drink{
    constructor(maDrink, nameDrink, price, dateCreate){
        this.maDrink = maDrink;
        this.nameDrink = nameDrink;
        this.price = price;
        this.dateCreate = dateCreate;
    }

    getMaDrink(){
        return this.maDrink;
    }
    getNameDrink(){
        return this.nameDrink;
    }
    getPrice(){
        return this.price;
    }
    getDateCreate(){
        return this.dateCreate;
    }
}
module.exports.Drink = Drink;
