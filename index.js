//khai báo thư viện express
const express = require("express");
const { Drink } = require("./Drink");

//khởi tạo app NodeJs
const app = express();

//khai báo cổng chạy ứng dụng
const port = 8000;

var drink1 = new Drink("TRATAC", "Trà tắc", 10000, "14/5/2021");
var drink2 = new Drink("COCA", "Cocacola", 15000, "14/5/2021");
var drink3 = new Drink("PEPSI", "Pepsi", 10000, "14/5/2021");
var drink = {
    drink1,
    drink2,
    drink3,
};
//get Api
app.get("/drink-list", (req, res) => {
    res.status(200).json({
        drink
    });
})
//get Api filter
app.get("/drink-list/:code", (req, res) => {
    var code = req.params.code;
    // // var drinkObj = JSON.parse(drink);
    console.log(drink);
    var drinkArr = Object.values(drink);
    var newDrink = drinkArr.filter(obj => {
        return obj.maDrink == code;
    });

    // var newDrink = JSON.stringify(newObj);

    res.status(200).json({
        code,
        newDrink
    });
})

//chạy ứng dụng trên cổng
app.listen(port, () => {
    console.log("App running on port: " + port);
})